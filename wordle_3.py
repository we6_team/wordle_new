import requests as rq
import random

DEBUG = False

class Wordle:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    create_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}

        try:
            reg_resp = self.session.post(Wordle.register_url, json=register_dict)
            reg_resp.raise_for_status()  

            self.me = reg_resp.json()['id']
            creat_dict = {"id": self.me, "overwrite": True}
            self.session.post(Wordle.create_url, json=creat_dict)
            self.tries = []

            self.choices = [w for w in Wordle.words if self.is_unique(w)]
            random.shuffle(self.choices)

        except rq.exceptions.RequestException as e:
            print(f"Failed to register or create game: {e}")
            raise

    def is_unique(self, word: str) -> bool:
        return len(word) == len(set(word))

    def play(self) -> None:
        def post(choice: str) -> tuple:
            guess = {"id": self.me, "guess": choice}
            try:
                response = self.session.post(Wordle.guess_url, json=guess)
                response.raise_for_status()  

                rj = response.json()
                right = rj["feedback"]
                status = "win" in rj.get("message", "").lower()
                return right, status
            except rq.exceptions.RequestException as e:
                print(f"Failed to make guess: {e}")
                raise

        try:
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            self.tries.append(f'{choice}:{right}')

            while not won:
                if DEBUG:
                    print(choice, right, self.choices[:10])
                self.update(choice, right)
                choice = random.choice(self.choices)
                self.choices.remove(choice)
                right, won = post(choice)
                self.tries.append(f'{choice}:{right}')

            self.end_game(choice)

        except rq.exceptions.RequestException as e:
            print(f"An error occurred during gameplay: {e}")
            raise

    def update(self, choice: str, feedback: str) -> None:
        def matches_feedback(word: str, choice: str, feedback: str) -> bool:
            if len(word) != len(choice):
                return False

            for i, (fc, c) in enumerate(zip(feedback, choice)):
                if fc == 'G':
                    if word[i] != c:
                        return False
                elif fc == 'Y':
                    if word[i] == c or c not in word:
                        return False
                elif fc == 'R':
                    if c in word:
                        return False
            return True

        print(f"Updating choices based on feedback {feedback} for choice {choice}")
        before_count = len(self.choices)

        self.choices = [w for w in self.choices if matches_feedback(w, choice, feedback)]

        after_count = len(self.choices)
        print(f"Choices reduced from {before_count} to {after_count}")

    def end_game(self, choice: str) -> None:
        print("Secret is", choice, "found in", len(self.tries), "attempts")
        print("Route is:", " => ".join(self.tries))
        if len(self.tries) > 6:
            print(f'Exceeded the number of attempts, the correct word was {choice}')


def main():
    game = Wordle("We6_Team")
    game.play()

main()    

