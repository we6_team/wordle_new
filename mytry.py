import requests as rq
import random

DEBUG = False

class MMBot:
    words = [word.strip() for word in open("newwords.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        

        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        
        
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)

        
        self.choices = [w for w in MMBot.words[:] if is_unique(w)]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            feedback = rj["feedback"]
            won = feedback == "GGGGG"
            return feedback, won

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']

        while not won:
            if DEBUG:
                print("Choice:", choice, "Feedback:", feedback, "Choices remaining:", len(self.choices))
            
            self.update(choice, feedback)
            
            if not self.choices:
                print("No valid choices left. Breaking out.")
                break
            
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
        
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))

    def update(self, choice: str, feedback: str):
        def matches_feedback(word: str, choice: str, feedback: str) -> bool:
            if len(word) != len(choice):
                return False

            for i, (fc, c) in enumerate(zip(feedback, choice)):
                if fc == 'G':
                    if word[i] != c:
                        return False
                elif fc == 'Y':
                    if word[i] == c or c not in word:
                        return False
                elif fc == 'R':
                    if c in word:
                        return False
            return True
        
        print(f"Updating choices based on feedback {feedback} for choice {choice}")
        before_count = len(self.choices)
        
        self.choices = [w for w in self.choices if matches_feedback(w, choice, feedback)]
        
        after_count = len(self.choices)
        print(f"Choices reduced from {before_count} to {after_count}")

# Create and play the game
game = MMBot("CodeShifu")
game.play()

