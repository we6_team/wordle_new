# Wordle Game Implementation

## Overview
This Python script implements a simple version of the Wordle game that interacts with a web-based game server via API requests. The game involves guessing a secret 5-letter word based on feedback provided by the server.

## Features
- **Player Registration**: The script registers a player on the game server using a provided name.
- **Game Initialization**: After registration, the script creates a new game session.
- **Word Guessing**: The script makes up to 6 guesses to determine the secret word.
- **Feedback Handling**: Feedback from the server (in the form of 'G', 'Y', 'R') is used to refine subsequent guesses.
- **Game Completion**: The game concludes with either a win (correctly guessing the word) or a loss (exceeding the guess limit).

## Requirements
- Python 3.x
- `requests` library for making HTTP requests (`pip install requests`)

## Usage
1. Ensure Python 3.x is installed on your system.
2. Install the required `requests` library using `pip` if not already installed: `pip install requests`.
3. Prepare a `words.txt` file containing a list of words, each on a new line.
4. Update the script with the correct `base_url` for the game server (`mm_url` in the script).
5. Run the script using Python: `python wordle.py`.

## Files
- **wordle.py**: Main Python script implementing the Wordle game.
- **words.txt**: Text file containing the list of words used for guessing.

## Troubleshooting
- Ensure the `base_url` (`mm_url`) in the script matches the actual game server endpoint.
- Check internet connectivity and server availability.
- Review error messages for issues with API requests (`POST` requests to `register`, `create`, `guess` endpoints).
