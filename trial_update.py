def update(self, choice: str, right: str):
        def choose(w: str, choice: str, right: str) -> bool:
            matches = []
            for pos, color in enumerate(right):
                if color == 'G' and w[pos] == choice[pos]:
                    matches.append(True)
                elif color == 'R' and choice[pos] not in w:
                    matches.append(True)
                elif color == 'Y' and choice[pos] in w:
                    matches.append(True)
            return len(matches) == 5
        self.choices = [w for w in self.choices if choose(w, choice, right) == True]
