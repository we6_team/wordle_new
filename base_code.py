import requests as rq
import random
DEBUG = False

class Wordle:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}

        reg_resp = self.session.post(Wordle.register_url, json=register_dict)

        reg_resp = self.session.post(Wordle.register_url, json=register_dict)

        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(Wordle.creat_url, json=creat_dict)
        self.tries = []

        self.choices = [w for w in Wordle.words[:] if is_unique(w)]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(Wordle.guess_url, json=guess)
            rj = response.json()
            right = rj["feedback"]
            status = "win" in rj["message"]
            return right, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        self.tries = [f'{choice}:{right}']

        while not won:
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            self.tries.append(f'{choice}:{right}')
        self.end_game(choice)

    def update(self, choice: str, feedback: str):
        def matches_feedback(word: str, choice: str, feedback: str) -> bool:
            if len(word) != len(choice):
                return False

            for i, (fc, c) in enumerate(zip(feedback, choice)):
                if fc == 'G':
                    if word[i] != c:
                        return False
                elif fc == 'Y':
                    if word[i] == c or c not in word:
                        return False
                elif fc == 'R':
                    if c in word:
                        return False
            return True
        
        print(f"Updating choices based on feedback {feedback} for choice {choice}")
        before_count = len(self.choices)
        
        self.choices = [w for w in self.choices if matches_feedback(w, choice, feedback)]
        
        after_count = len(self.choices)
        print(f"Choices reduced from {before_count} to {after_count}")
    

    def end_game(self, choice: str ):
      print("Secret is", choice, "found in", len(self.tries), "attempts")
      print("Route is:", " => ".join(self.tries))
      if len(self.tries) > 6:
          print(f'Exceeded the number of attepts, the correct word was {choice}')


game = Wordle("We6_Team")
game.play()
